FROM debian
RUN apt-get update && apt-get install -y less vim rsyslog build-essential libssl-dev libffi-dev python3 python3-pip python-dev virtualenv python3-venv
RUN echo "Europe/Kiev" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata
RUN pip3 install django
RUN python3 -m pip install --upgrade pip
RUN python3 -m venv myvenv
CMD source myvenv/bin/activate
EXPOSE 7777
ENTRYPOINT bin/bash
# CMD python3 /home/manage.py createsuperuser admin/A1qwerty
# CMD python3 /home/manage.py makemigrations
# CMD python3 /home/manage.py migrate
# CMD python3 /home/manage.py runserver 0.0.0.0:7777
