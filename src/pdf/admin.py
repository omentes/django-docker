from django.contrib import admin
from .models import Answer
from .models import Person
from .models import Spuse
from .models import Children
# Register your models here.

class ChildrenInline(admin.TabularInline):
    model = Children
    extra = 1
    max_num = 6
    # exclude = ['index']

class AnswerAdmin(admin.ModelAdmin):
    list_display = ('user', 'case_number', 'person_name', 'spuse_name', 'court_number', 'county',  'index')
    inlines = [ChildrenInline]
    def childrens(self, obj):
        return "\n".join([p.name for p in obj.child_name.all()])

class PersonAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'middle_name', 'last_name')


class SpuseAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'middle_name', 'last_name')

class ChildrenAdmin(admin.ModelAdmin):
	pass

admin.site.register(Answer, AnswerAdmin)
admin.site.register(Person, PersonAdmin)
admin.site.register(Spuse, SpuseAdmin)
admin.site.register(Children, ChildrenAdmin)