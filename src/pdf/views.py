from django.shortcuts import render
from .models import *

# This is where the data files are. Adjust as necessary.


def home(request):
    allAnswer = Answer.objects.all() 
    return render(request, 'index.html', locals())

