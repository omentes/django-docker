from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from PDFlib.PDFlib import PDFlib
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def createmypdf(object):
    infile = os.path.join(BASE_DIR, 'templates/my_pdf.pdf')
    searchpath = os.path.join(BASE_DIR, 'data/')
    outputfilename = "/static/docs/" + str(object.case_number)  + str(object.court_number) + ".pdf"
    p = PDFlib()
    try:
        p.set_option("errorpolicy=return")
        p.set_option("SearchPath={{" + searchpath +"}}")
        if p.begin_document("/home" + outputfilename, "") == -1:
            raise Exception("Error: 0")

        p.set_info("Creator", str(object.user))
        p.set_info("Author", str(object.user))
        p.set_info("Title","PDFlib block processing sample (C)")
        blockcontainer = p.open_pdi_document(infile, "")

        if blockcontainer == -1:
            raise Exception("Error: 1")

        page = p.open_pdi_page(blockcontainer, 1, "")
        if page == -1:
            raise Exception("Error: 2")

        p.begin_page_ext(0, 0, "")           # dummy page size

        # This will adjust the page size to the block container's size.
        p.fit_pdi_page(page, 0, 0, "adjustpage")
        # Fill all text blocks with dynamic data
        # plain ASCII text */

        #  FILL CASE NUMBER */
        p.setcolor("fill", "white", 1.0, 0.0, 0.0, 0.0)
        p.rect(280, 720, 150, 20)
        p.fill()

        # FILL YOUR NAME */
        p.setcolor("fill", "white", 1.0, 0.0, 0.0, 0.0)
        p.rect(125, 675, 150, 20)
        p.fill()

        # FILL COURT NUMBER */
        p.setcolor("fill", "white", 1.0, 0.0, 0.0, 0.0)
        p.rect(400, 690, 120, 10)
        p.fill()

        # FILL SPUSE NAME */
        p.setcolor("fill", "white", 1.0, 0.0, 0.0, 0.0)
        p.rect(125, 625, 150, 20)
        p.fill()

        # FILL COUNTY */
        p.setcolor("fill", "white", 1.0, 0.0, 0.0, 0.0)
        p.rect(385, 625, 70, 15)
        p.fill()

        # FILL CHILD 1 */
        p.setcolor("fill", "white", 1.0, 0.0, 0.0, 0.0)
        p.rect(80, 578, 100, 17)
        p.fill()

        # FILL CHILD 2*/
        p.setcolor("fill", "white", 1.0, 0.0, 0.0, 0.0)
        p.rect(230, 580, 100, 20)
        p.fill()

        # FILL FINAL DECREE 2*/
        p.setcolor("fill", "white", 1.0, 0.0, 0.0, 0.0)
        p.rect(40, 40, 530, 510)
        p.fill()

        optlist12 = "fontname={LinLibertine_R} embedding fontsize=12 encoding=unicode fillcolor={rgb 0.9 0.0 0.0}"
        optlist14 = "fontname={LinLibertine_R} embedding fontsize=14 encoding=unicode fillcolor={rgb 0.9 0.0 0.0}"
        p.fit_textline(str(object.case_number), 280, 721, optlist14)
        p.fit_textline(str(object.person_name), 125, 675, optlist14)
        p.fit_textline(str(object.court_number), 400, 690, optlist12)
        p.fit_textline(str(object.county), 385, 625, optlist12)
        p.fit_textline(str(object.spuse_name), 135, 625, optlist14)
        coordinates = [{'x': 80, 'y': 580}, {'x': 230, 'y': 580}, {'x': 380, 'y': 580}, {'x': 80, 'y': 563}, {'x': 230, 'y': 563}, {'x': 380, 'y': 563}]
        i = 0

        for item in object.answer_key.all():

            p.fit_textline(item.name, coordinates[i]['x'], coordinates[i]['y'], optlist12)
            i += 1

        p.end_page_ext("")
        p.close_pdi_page(page)

        p.end_document("")
        p.close_pdi_document(blockcontainer)

    except Exception:
        print("Exception occurred.")
    finally:
        p.delete()
    object.url = "/docs/static/" + str(object.case_number)  + str(object.court_number) + ".pdf"

class Spuse(models.Model):
    first_name = models.CharField(max_length=50, default="", blank=True, null=True)
    middle_name = models.CharField(max_length=50, default="", blank=True, null=True)
    last_name = models.CharField(max_length=50, default="", blank=True, null=True)

    class Meta:
        db_table = "spuse"
        verbose_name = "spuse"
        verbose_name_plural = "spuses"

    def __str__(self):
        return "{0} {1} {2}".format(self.first_name, self.middle_name, self.last_name)

class Person(models.Model):
    first_name = models.CharField(max_length=50, default="", blank=True, null=True)
    middle_name = models.CharField(max_length=50, default="", blank=True, null=True)
    last_name = models.CharField(max_length=50, default="", blank=True, null=True)

    class Meta:
        db_table = "person"
        verbose_name = "peson"
        verbose_name_plural = "persons"

    def __str__(self):
        return "{0} {1} {2}".format(self.first_name, self.middle_name, self.last_name)

class Answer(models.Model): 
    user = models.ForeignKey(User, related_name='answers', blank=True, null=True, on_delete=models.CASCADE)
    question_name = models.CharField(max_length=255, default="")
    case_number = models.CharField(max_length=10, default="")
    person_name = models.ForeignKey(Person, on_delete=models.CASCADE)
    spuse_name = models.ForeignKey(Spuse, blank=True, null=True, on_delete=models.CASCADE)
    court_number = models.CharField(max_length=10, default="")
    county = models.CharField(max_length=25, default="")
    url = models.CharField(max_length=100, default="", blank=True, null=True)
    value = models.TextField(blank=True, null=True)
    index = models.PositiveIntegerField(default=0)

    class Meta:
        db_table = "answer"
        verbose_name = "answer"
        verbose_name_plural = "answers"

    def save(self, *args, **kwargs):
        t = 1
        child = Children.objects.filter(answer_key=self)
        for i in child:
            i.index = t
            i.save()
            t += 1
        createmypdf(self)
        super(Answer, self).save(*args, **kwargs)

class Children(models.Model):
    name = models.CharField(max_length=50, default="", blank=True, null=True)
    bith_day = models.DateField(blank=True, null=True)
    answer_key = models.ForeignKey(Answer, related_name="answer_key", null=True, on_delete=models.CASCADE)
    index = models.PositiveIntegerField(default=0)

    class Meta:
        db_table = "child"
        verbose_name = "child"
        verbose_name_plural = "childrens"

    def __str__(self):
        return "{0}".format(self.name)
    